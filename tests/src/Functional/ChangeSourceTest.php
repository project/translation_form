<?php

namespace Drupal\Tests\translation_form\Functional;

use Drupal\Tests\node\Functional\NodeTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;

/**
 * Class ConfigsTest.
 *
 * @package Drupal\Tests\translation_form\Functional
 *
 * @group translation_form
 */
class ChangeSourceTest extends NodeTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['translation_form'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The added languages.
   *
   * @var string[]
   */
  protected $langcodes;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $langcodes = ['de', 'fr'];
    foreach ($langcodes as $langcode) {
      ConfigurableLanguage::createFromLangcode($langcode)->save();
    }

    $user = $this->drupalCreateUser([
      'administer site configuration',
      'administer nodes',
      'create article content',
      'edit any article content',
      'delete any article content',
      'administer content translation',
      'translate any entity',
      'create content translations',
      'administer languages',
      'administer content types',
    ]);
    $this->drupalLogin($user);

    // Enable translation for the current entity type and ensure the change is
    // picked up.
    \Drupal::service('content_translation.manager')->setEnabled('node', 'article', TRUE);
    \Drupal::entityTypeManager()->clearCachedDefinitions();
    \Drupal::service('router.builder')->rebuild();

    // Enable allow to change source language on edit page.
    $edit = [
      'allow_to_change_source_language' => TRUE,
    ];
    $this->drupalGet(Url::fromRoute('translation_form.settings_form'));
    $this->submitForm($edit, t('Save configuration'));
  }

  /**
   * Tests check if user can see and save source language on edit entity form.
   */
  public function testSourceLanguageFormElementsExistence() {
    // Create a node in English.
    $node = $this->drupalCreateNode(['type' => 'article', 'langcode' => 'en']);

    // Create a translation in German.
    $this->drupalGet($node->toUrl('drupal:content-translation-overview'));
    $this->clickLink('Add');
    $this->submitForm([], t('Save (this translation)'));

    // Create a translation in French.
    $this->drupalGet($node->toUrl('drupal:content-translation-overview'));
    $this->clickLink('Add');
    $this->submitForm([], t('Save (this translation)'));

    // Go to edit page for French language and change source language.
    $this->drupalGet($node->toUrl('drupal:content-translation-overview'));
    $this->clickLink('Edit', 1);
    $edit = [
      'source_langcode[source]' => 'de',
    ];
    $this->submitForm($edit, t('Save (this translation)'));

    // Check source language for tranlsate for node.
    $this->drupalGet($node->toUrl('drupal:content-translation-overview'));
    $elements = $this->xpath('//tbody/tr[2]/td[3]/text()');
    $this->assertEquals($elements[0]->getText(), t('German'), new FormattableMarkup('Source language is correct for %language translation.', ['%language' => 'French']));
  }

}
